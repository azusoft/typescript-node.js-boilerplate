# README #

Typescript Node.JS Boilerplate

### BUILD ###

```sh
$ yarn install
$ yarn build
```

### TESTS ###

```sh
$ yarn install
$ yarn test
```

### RUN WITHOUT BUILD ###

```sh
$ yarn install
$ yarn start-dev
```