import { Person } from '../source/utility';
import { expect } from 'chai';
import 'mocha';

describe('Concatenate function', () => {
	it('should return David Smith', () => {
		let David: Person = new Person('David', 'Smith');
		expect(David.Concatenate()).to.equal('David Smith');
	});
});